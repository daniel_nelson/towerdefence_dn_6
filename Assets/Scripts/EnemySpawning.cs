﻿using UnityEngine;
using System.Collections;

public class EnemySpawning : MonoBehaviour
{

	public Vector3 spawnerPosition;
	private Vector3 spawnRandom;
	GameObject enemyClone;
	public GameObject simpleEnemy;
	Grid grid;
	Pathfinder pathfinder;
	// Use this for initialization
	void Start()
	{
		spawnerPosition = GameObject.Find("SpawnZone").transform.position;




	}
	void UpdateSpawnPos(){
		spawnRandom.x = spawnerPosition.x + (Random.Range (-4, 4));
		spawnRandom.z = spawnerPosition.z + (Random.Range (-4, 4));
		spawnRandom.y = spawnerPosition.y;
	}
	// Update is called once per frame
	void Update()
	{
		if (Input.GetKeyDown (KeyCode.F)) {	
			UpdateSpawnPos ();

			enemyClone = Instantiate(simpleEnemy, spawnRandom, Quaternion.identity) as GameObject;
			enemyClone.GetComponent<BasicEnemy>().setAtributes (5, 10, 1, 5, BasicEnemy.targetStateEnum.targetPlayer);

		}
		if(Input.GetKeyDown(KeyCode.T)){
			UpdateSpawnPos ();

			enemyClone = Instantiate (simpleEnemy, spawnRandom, Quaternion.identity) as GameObject;			
			enemyClone.GetComponent<BasicEnemy>().setAtributes (5, 10, 1, 5, BasicEnemy.targetStateEnum.targetBase);

		}
	}
}
