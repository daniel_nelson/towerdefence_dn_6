﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TowerTracking : MonoBehaviour
{

    public float speed = 5;
    public GameObject target;
    GameObject first = null;
    public GameObject EndZone;
    Vector3 lastKnownPosition = Vector3.zero;
    Quaternion lookAtRotation;
    public int trackRadius;
    SphereCollider sc;
    List<GameObject> trackingList = new List<GameObject>();
    float distance;
    public int trackCount;

    // Use this for initialization
    void Start()
    {
        sc = GetComponent<SphereCollider>();
        sc.radius = trackRadius;
        distance = Mathf.Infinity;
        EndZone = GameObject.FindWithTag("EndZone");
    }

    // Update is called once per frame
    void Update()
    {

        //target = GameObject.FindWithTag("Enemy");
        trackCount = trackingList.Count;
        if (trackCount > 0)
        {
            target = FindFirst();
            //target = GameObject.FindWithTag("Enemy");
        }
        else
        {
            target = null;
        }



        if (target != null)
        {

            //target = FindFirst();
            //if ((Vector3.Distance(transform.position, target.transform.position) < maxRange)
            //&& (Vector3.Distance(transform.position, target.transform.position) > minRange))

            if (lastKnownPosition != target.transform.position)
            {
                lastKnownPosition = target.transform.position;
                lookAtRotation = Quaternion.LookRotation(lastKnownPosition - transform.position);
            }
            if (transform.FindChild("Pivot").rotation != lookAtRotation)
            //if (transform.rotation != lookAtRotation)
            {
                //transform.rotation = Quaternion.RotateTowards(transform.rotation, lookAtRotation, speed * Time.deltaTime);
                transform.FindChild("Pivot").FindChild("barrel").Rotate(0, 0, 30f);
                transform.FindChild("Pivot").rotation = Quaternion.RotateTowards(transform.FindChild("Pivot").rotation, lookAtRotation, speed * Time.deltaTime);
            }

        }
        else
        {
            target = null;
        }
        print(trackingList.Count);

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            trackingList.Add(other.gameObject);


        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Enemy")
        {
            trackingList.Remove(other.gameObject);
        }
    }

    public GameObject FindFirst()
    {
        distance = Mathf.Infinity;
        foreach (GameObject enemy in trackingList)
        {
            if (enemy)
            {
                Vector3 diff = enemy.transform.position - EndZone.transform.position;
                float curDistance = diff.sqrMagnitude;
                if (curDistance < distance)
                {
                    first = enemy;
                    distance = curDistance;
                    //target = go;

                }
            }
            else
            {
                trackingList.Remove(enemy);
            }
        }
        return first;
    }


}
